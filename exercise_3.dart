void main() {
  // create a variable to intantiate a new object of AppAward
  var award = new AppAward();

  // initialize the new object
  award.name = "Ambani";
  award.category = "Educational";
  award.developer = "Mukundi Lambani";
  award.year = 2021;

  // call the printAPpInfo function to print the information passed
  award.printAppInfo();
}

// declare a class AppAward
class AppAward {
  String? name;
  String? category;
  String? developer;
  int? year;

  // create a function to capitalize the app anem
  String capNameLetters(String appName) {
    appName = this.name!.toUpperCase();
    return appName;
  }

  // create a function to print the app award information
  void printAppInfo() {
    print("App Name: " + capNameLetters(name!));
    print("App Category: $category");
    print("App Developer by: $developer");
    print("App Year: $year");
  }
}
