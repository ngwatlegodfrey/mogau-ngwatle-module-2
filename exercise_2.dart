import 'dart:collection';

void main() {
  // declaring an array for app names and initializing
  var appNames = [
    "FNB Banking",
    "SnapScan",
    "Live Inspect",
    "WumDrop",
    "Domestly",
    "Shyft",
    "Khula",
    "Naked Insurance",
    "EasyEquities",
    "Ambani"
  ];
  // declaring an array for years and initializing
  var year = [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021];

  // declaring a Map and use Map to intialize the app and year names
  var arrWinningApps = Map<int, String>.fromIterables(year, appNames);

  var sortedByValue = new SplayTreeMap<int, String>.from(arrWinningApps,
      (key1, key2) => arrWinningApps[key1]!.compareTo(arrWinningApps[key2]!));

  print(
      "-------------------------------------------------------------------------");
  print("a) SORTED APPS BY NAME: ");
  for (var value in sortedByValue.values) print("-" + value);
  print(
      "-------------------------------------------------------------------------");
  if (arrWinningApps.keys.elementAt(5) == 2017 &&
      arrWinningApps.keys.elementAt(6) == 2018) {
    print("b) THE WINNING APPS OF 2017 AND 2018 ARE: " +
        sortedByValue[2017]! +
        " and " +
        sortedByValue[2018]!);
  } else {
    print("Nothing found");
  }
  print(
      "-------------------------------------------------------------------------");

  print(
      "-------------------------------------------------------------------------");
  print("c) The total number of apps are: " + arrWinningApps.length.toString());
  print(
      "-------------------------------------------------------------------------");
}
